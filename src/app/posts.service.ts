import {Injectable} from "@angular/core";
import {PostInterface} from "./Post.interface";
import {HttpClient, HttpHeaders, HttpParams, HttpEventType} from "@angular/common/http";
import {map, catchError, tap} from "rxjs/operators";
import {Subject, throwError} from "rxjs";

@Injectable({providedIn: "root"})
export class PostsService {
  serverURL = 'https://http-01-start-ca08e.firebaseio.com/';
  error = new Subject<string>()

  constructor(private http: HttpClient) {

  }
  createStorePost(postData){
    return this.http
      .post<{[key: string]: string}>(
        this.serverURL + 'posts.json',
        postData
      ).subscribe(responseData => {
        console.log(responseData);

      }, errorObject =>{
            this.error.next(errorObject);
      } );

  }

  fetchPost(){
    return this.http.get<{[key: string]: PostInterface}>(this.serverURL + 'posts.json',
      {
        headers: new HttpHeaders({
                'custom-header' : ' I am custom'
          }),
        params: new HttpParams().set('print', 'pretty')
      })
      .pipe(map((data)=>{
        let resArray: PostInterface[] = [];
        for(let i in data){
          if(data.hasOwnProperty(i)) {
            resArray.push({...data[i], id: i})
          }
        }
        return resArray;
      }), catchError((errorResponse) => {
          // generaric error handling
        return throwError(errorResponse);
      })
    );
  }

  deleteAllPosts(){
    return this.http.delete(this.serverURL+ 'posts.json', {
      observe: "events",
      responseType: 'text'
    }).pipe(tap((event)=>{
        console.log('event:', event)
      if(event.type == HttpEventType.Response){
        console.log(event.body)
      }
      if(event.type == HttpEventType.Sent){
        console.log("event is not a response")
      }
    }))
  }
}
