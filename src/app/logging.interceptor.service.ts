import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

export class LoggingInterceptorService implements HttpInterceptor{

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Request in logging interceptor')
    console.log('Req Url: ', req.url)
    // manipulating response
    return next.handle(req).pipe(tap(event=>{
      console.log(event)
      if (event.type == HttpEventType.Response){
        console.log('response arrived in logging interceptor, data: ', event.body)
      }
    }));
  }

}
