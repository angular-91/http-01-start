import {Component, OnDestroy, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {PostInterface} from "./Post.interface";
import {PostsService} from "./posts.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loadedPosts: PostInterface[] = [];
  isFetching = false
  error = false
  errorSub: Subscription = new Subscription();

  constructor(private http: HttpClient,
              private postsService: PostsService) {}

  ngOnInit() {
    this.fetchPosts()
    this.errorSub = this.postsService.error.subscribe((errorObject)=>{
      console.log('Error while creating new post: ', errorObject);
    })
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }

  onCreatePost(postData: { title: string; content: string }) {
    // Send Http request
    this.postsService.createStorePost(postData)
    this.fetchPosts()

  }

  onFetchPosts() {
    // Send Http request
    this.fetchPosts()
  }

  onClearPosts() {
    // Send Http request
    this.postsService.deleteAllPosts().subscribe(()=>{
      this.loadedPosts = [];
      console.log("deleted")
    })
  }

  private fetchPosts(){
    this.isFetching = true;
    this.postsService.fetchPost().subscribe((post: PostInterface[])=>{
      this.loadedPosts = post
      this.isFetching = false;
    }, (error)=>{
        this.isFetching = false;
        console.log(error)
        this.error = error.message;
    })
  }

  onHandleError(){
    this.error = false;
  }
}
